# workflow-engineer-training-viacheslav-glekov

# Introducing into markdown.

<p> Hello everyone. I'm Viacheslav Glekov, student of BSUIR university.</p>

<p>In my spare time, I do programming . I'm currently learning Python. I can solve a wide range of tasks using this technology, from console applications to web applications using django and gui applications using tkinter.</p>

<p>In leisure time I also like to read various articles about the news in the world of IT technologies to expand my horizons.</p>


<h3>My skills scoreboard</h3>

| technology |  mark |
| ------     | ------|
| python     |    3  |
| MySQL      |    2  |
| YAML       |    2  |
| csv        |    2  |
| md         |    1  |
| git        |    2  |
| redmine    |    0  |
| jira       |    0  |


<h3>My schedule</h3>

| weekday    |  working hours |
| ------     | ------         |
| Monday     |   12:00-20:00  |
| Tuesday    |   10:00-18:00  |
| Wednesday  |   13:00-21:00  |
| Thursday   |   10:00-18:00  |
| Friday     |   12:00-20:00  |
| Saturday   |   9:00-13:00   |
| Sunday     |       -        |



You can find me by following the links below <br>

<h3>[GitHub](https://github.com/vglekov) <br>
[GitLab](https://gitlab.com/v.glekov) <br>
[Jira](https://workflow-engineers-syberry.atlassian.net/jira/people/604cf0613ab18c00680749ab) <br>
[VK](https://vk.com/viacheplavik) <br>
Discord: Glekov Viacheslav#2773 <br></h3>



