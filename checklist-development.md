# Feature engineering guidelines checklist

## Read the requirements 

- Make sure that you understand every sentence and every word in both functional and non-functional requirements defined for the task.

- Communicate and address every questions and potential misunderstanding you have before starting the coding.

- Suggest workarounds and try to make your own decision if some non-critical information is not explicitly defined in the requirements.

- Capture all your questions, workarounds and decision in the task tracker.

- Be able to explain how you are going to confirm that the features address all requirements, both functional, non-functional and architectural.

- Suggest simpler implementation or changes in requirements to simplify the implementation if it does not contradict task's goals.
 
## Implement the feature 

- The code shall cover both positive and negative scenarios.

- The code shall work properly with edge cases of the potential input.

- The code shall properly handle errors in input data and 3rd party system call responses.

-  In case of errors the system shall either continue to work and write error information into logs or shutdown and provide a clear response to user with the explanation of the source of error.

- The code shall log the necessary information for potential troubleshooting.

- Reproduce the issue and understand the root cause before fixing it.

- Look for ways to automate your work and avoid creating repetitive boilerplate code.

- Push the code at least daily.

- Update the technical documentation of the system.

  
## Communicate 

- If some dependency for the complete and successful implementation is not working (backend API, infrastructural configuration, etc.) it is your responsibility to communicate with the owner of the dependency (backend developer, devops engineer, project manager, etc.) to push your feature to the completion.

- Add helpful information for the QA team on how to better test the implementation to the ticket tracker (potential impact in the complex system, which areas may be affected, how to access the functionality that does not have UI, etc.)

- Notify lead engineer or project manager if the implementation takes longer that you planned.

- Ask for help if you stuck and do not know how to proceed with the implementation.


## Test the feature 

- It is your responsibility to test your code and provide production ready implementation.

- Make sure the code builds without any errors and warnings (including coding guidelines style checks).

- Do not push the code that you did not run.

- Test all positive and negative scenarios.

[Redmine](https://academy-redmine.jarvis.syberry.net/issues/98514)

